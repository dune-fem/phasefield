cd ~/DUNE
if python -c "import phasefield" >& /dev/null; then
  echo "#################################################################"
  echo "this script only needs to be run once within the docker container"
  echo "and this seems to have happened already...."
  echo "#################################################################"
  cd /host
  exit 0
fi

git clone -b releases/2.6 https://gitlab.dune-project.org/staging/dune-typetree
git clone -b releases/2.6 https://gitlab.dune-project.org/staging/dune-functions
git clone https://git.imp.fu-berlin.de/agnumpde/dune-matrix-vector.git
git clone https://git.imp.fu-berlin.de/agnumpde/dune-fufem.git
git clone https://git.imp.fu-berlin.de/agnumpde/dune-solvers.git
git clone https://git.imp.fu-berlin.de/agnumpde/dune-tnnmg.git

cd dune-matrix-vector
git checkout 1afe098a6c130c3ef0e241f18437ae8f81c84fcb
cd ..

cd dune-solvers
git checkout b7acfccf2bdbdc34816a4d5f48f0fccb9db21b98
cd ..

cd dune-tnnmg
git checkout 016ec9e0101c8e1af767405597c7129cf6da74ad
cd ..

cd dune-fufem
git checkout 7288887c59f2bee201e425b3597f3ce55f15f6e2
git apply /host/patches/*.patch
cd ..

./dune-common/bin/dunecontrol --opts=config.opts --only=dune-typetree all
./dune-common/bin/dunecontrol --opts=config.opts --only=dune-functions all
./dune-common/bin/dunecontrol --opts=config.opts --only=dune-matrix-vector all
./dune-common/bin/dunecontrol --opts=config.opts --only=dune-fufem all
./dune-common/bin/dunecontrol --opts=config.opts --only=dune-solvers all
./dune-common/bin/dunecontrol --opts=config.opts --only=dune-tnnmg all

./dune-python/bin/setup-dunepy.py --opts=config.opts install

mkdir phasefield
cd phasefield
cp -Lr /host/phasefield .
cp /host/setup.py .
cp /host/README.md .
pip install --upgrade -e .
cd /host
