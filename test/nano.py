import sys
from ufl import TestFunction, TrialFunction, conditional, Coefficient, Constant, triangle, SpatialCoordinate, as_vector, tanh, inner, dx, pi, grad, inner, outer, elem_mult, elem_div, elem_pow, conditional, sqrt
import math
import dune.create as create
from dune.grid import Marker, cartesianDomain
import dune.fem as fem
import numpy as np
from dune.ufl import NamedConstant, expression2GF
from dune.generator import algorithm
from dune.fem.function import levelFunction, partitionFunction, integrate
import random


dimDomain = 2     # we are solving this in 2d
dimRange = 1
order = 1

t = 0.0

domain = cartesianDomain([-0.5,-0.5],[0.5,0.5],[3,3])
grid   = create.view("adaptive",grid="ALUConform", constructor=domain, dimgrid=dimDomain)

space = create.space("lagrange", grid, dimrange=dimRange, order=order, storage="istl")


A = NamedConstant(triangle,"A")
B = NamedConstant(triangle,"B")

f = expression2GF(grid,2*A,order=4)
g = expression2GF(grid,A+B,order=4)

while t < 0.2:

    #carry out integrations
    f.setConstant("A",[random.random()])
    g.setConstant("B",[f.integrate()[0]])
    g.setConstant("A",[random.random()])
    result = g.integrate()[0]

    print("g is: ",result)
    t +=  0.01

