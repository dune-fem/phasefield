#!/sr/bin/python
"""
Mean curvature flow computation with implicit potential well.
"""

from ufl import conditional, as_vector, triangle,\
                TrialFunction, TestFunction, Coefficient, VectorElement, FiniteElement
from phasefield import PhaseModel

class Mcf:
    """Sharp definition for mean curvature flow."""
    # dimDomain = 2
    omega = [[0,0],[2,2],[10,10]]
    endTime = 0.125
    saveStep = 0.001
    mobility = 1
    def initial(x):
        """
        Initial conditions
        """
        return [[conditional(x[0]*x[0]+x[1]*x[1] < 0.25, 1, 0),
                 conditional(x[0]*x[0]+x[1]*x[1] > 0.25, 1, 0)]]
    def gamma(nu):
        """
        Matrix of surface tensions
        """
        return [[0, 1], [1, 0]]

if __name__ == "__main__":
    # construct the phase field class with given values for `epsilon` and # `dt`
    phaseField = PhaseModel(Mcf, epsilon=0.03, dt=0.001)

    # we need a space with two components since the model is setup
    # with two phase field variables
    space = VectorElement("Lagrange", triangle, 1, 2)
    vPhi = TrialFunction(space)
    phi = Coefficient(space)
    phiN = Coefficient(space)
    form = phaseField.setupPhase(phi, phiN, None, None, vPhi, None)

    # alternatively we can use a one dimensional space and pass in
    # (phi,1-phi) to the `setupPhase` method
    space = FiniteElement("Lagrange", triangle, 1)
    vPhi = TrialFunction(space)
    phi = Coefficient(space)
    phiN = Coefficient(space)
    extend = lambda p: as_vector([p,1-p])
    form = phaseField.setupPhase(extend(phi), extend(phiN), None, None, as_vector([vPhi,0]), None)
