#!/usr/bin/python
"""
Mean curvature flow with obstacle potential well and uniform refinement
"""
import ufl
from ufl import pi, sqrt
from phasefield import PhaseStepper, PhaseModel
from phasefield.auxfun import ConcaveConvex

from dune.fem.plotting import plotComponents
from dune.grid import cartesianDomain

from dune.fem.function import integrate

def compute(baseName, maxLevel, epsilon, dt):
    r0 = 0.5
    class Mcf:
        """Sharp definition for mean curvature flow"""
        omega = cartesianDomain([-2, -2], [2, 2], [3, 3])
        endTime = 0.125
        saveStep = 0.001
        fileBase = "McfCC"
        mobility = 1
        def initial(x):
            """
            Initial conditions
            """
            return [[0.5-0.5*ufl.tanh((1/epsilon)*(x[0]*x[0]+x[1]*x[1]-r0*r0)),
                     0.5+0.5*ufl.tanh((1/epsilon)*(x[0]*x[0]+x[1]*x[1]-r0*r0))]]

        def gamma(nu):
            """
            Matrix of surface tensions
            """
            return [[0, 1], [1, 0]]

    phaseField = PhaseModel(Mcf, well=ConcaveConvex, epsilon=epsilon, dt=dt)

    fempyBase = PhaseStepper(phaseField)

    fempyBase.gridSetup(13, maxLevel)
    fempyBase.defaultRefine = [1.4, 1.2, 4, maxLevel]

    solution = fempyBase.solution
    radiusVec = []
    while fempyBase.time < Mcf.endTime:
        if fempyBase.saveTime is not None and fempyBase.saveTime <= fempyBase.time +dt:
            print("saving")
            radius = sqrt(integrate(fempyBase.gridView,solution,4)[0]/pi)
            radiusVec.append(radius)
        fempyBase.nextTime()
        fempyBase.adapt()
    return radiusVec

import numpy as np
import matplotlib.pyplot as plt

maxLevel = 13
dt = 1e-5

try:
    myDict = np.load('mcfCCsim.npy', allow_pickle='TRUE').item()
except FileNotFoundError:
    print("Creating a new dictionary for saving")
    myDict = {}
    t = 0
    exactList = []
    while t < 0.125:
        exactList.append(sqrt(0.5*0.5-2*t))
        t += dt
    myDict['exact'] = exactList

#for eps in np.linspace(0.03, 0.02, 6):
for eps in np.linspace(0.09, 0.07, 3):
    if (maxLevel, eps, dt) in myDict:
        print("sim found in dict using old value")
    else:
        print("sim not found in dict running")
        try:
            myDict[(maxLevel, eps, dt)] = compute("cc", maxLevel, eps, dt)
            np.save('mcfCCsim.npy', myDict)
        except Exception as e:
            print(e)
            myDict[(maxLevel, eps, dt)] = False
            print("simulation", (maxLevel, eps, dt), "failed")

for key, value in myDict.items():
    if value != False:
        plt.plot(value, label=key)

np.save('mcfCCsim.npy', myDict)

plt.legend()
plt.show()
