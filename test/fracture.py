"""
Frcature results inconsistent results
"""
from phasefield.auxfun import InterpolateOne
from phasefield import PhaseStepper, PhaseModel

from dune.fem.function import levelFunction, partitionFunction
from dune.fem.utility import pointSample

from ufl import as_vector, conditional, Identity, dot, inner, grad, tr, transpose, sqrt, tan, pi, sin, cos

#************ HELP FUNCTIONS *****************************
def eps(u):
    return 0.5*(grad(u)+transpose(grad(u)))

def sigma(u):
    lam = mu = 22000
    geodim = 2
    return lam*tr(eps(u))*Identity(geodim) + 2*mu*eps(u)
#*********************************************************

class Fracture:
    omega = ("../data/crack2.dgf", 2)
    endTime = 0.1
    mobility = 2e-3
    saveStep = 0.

    tipCenter = [25,0]
    tipRadius = 0.01

    def dirichlet(t, x):
        #return {2: [0, 0], 3: [None, 0]}
        return {2: [0, 0], 3: [None, 0]}

    def neuman(t,x):
        return as_vector([0,conditional(x[1]>75-1e-8,-t*6,0)])

    def initial(x):
        tipCenter = as_vector(Fracture.tipCenter)
        tipRadius = Fracture.tipRadius
        crack = 1 - conditional(x[1] > tipRadius, 0, 1) * conditional(x[0] > tipCenter[0], 0, 1)
        return [[crack], [0, 0]]

    def gamma(nu):
        return [[0.5]]

    def a(u, gradu):
        return [0.5*inner(sigma(u), eps(u))]

    def distE(u):
        return [[0], [0]]

    def distQ(u):
        return [[sigma(u)[0, :]], [sigma(u)[1, :]]]

    def distF(u, x):
        return [[0], [0]]

def compute(sim, maxLevel, epsilon, dt, crackRes, eta,mob):

    def newgamma(nu):
        return [[crackRes/2]]

    Fracture.gamma = newgamma
    Fracture.mobility = mob

    #TODO make single well dependent on gamma here which is the 0.5
    ### mynote: what is the 's' in the well argument
    uflModel = PhaseModel(Fracture, epsilon=epsilon, dt=dt, constrained=True)

    uflModel.bulkSmoothdict['Q', 0] = lambda phi, phiK: [eta + phiK[0] * phiK[0]]
    uflModel.bulkSmoothdict['Q', 1] = lambda phi, phiK: [eta + phiK[0] * phiK[0]]

    uflModel.interpolate = InterpolateOne

    fempyBase = PhaseStepper(uflModel,
                         preBulk = [0,1],
                         solver="cg",
                         storage="istl")
    solution = fempyBase.solution
    fempyBase.indicator = 1-solution[0]
    ### mynote: perhaps call adapt or call 'adapt' method 'refine'...
    #fempyBase.defaultRefine = [3e-1, 1e-1, 8, maxLevel]
    fempyBase.defaultRefine = [2e-1, 1e-1, 4, maxLevel]
    fempyBase.gridSetup(8,maxLevel,smoothingEpsilon=6)

    dx = 0.5
    point = Fracture.tipCenter[:]
    point[0] += dx/2
    arrivalTimes = []

    # set maximum for time

    while fempyBase.time <= 12: # fempyBase.time < Fracture.endTime:
        print(fempyBase.time)
        fempyBase.nextTime()
        fempyBase.adapt()
        value = pointSample(solution,point)
        if value[0] < 0.05:
            break
    return fempyBase.time

if __name__== "__main__":
    import numpy as np
    import matplotlib.pyplot as plt

    eta = 1e-5
    mob = 2e-3

    epsilon = 0.625
    maxLevel = 11
    dt = 1e-2

    import math

    results = []

    #for crackRes in [9.5,8.5,9.5]:
    for crackRes in [8.5,9.5,8.5]:
        temp = compute("",maxLevel,epsilon,dt,float(crackRes), eta,mob)
        results.append(temp)

    print(results)
