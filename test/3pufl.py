from phasefield.external import External

#u[0],u[1],u[2] and un
# v
# set dt
# self epsilon
mobility = 10
Gamma = [1., 1., 1.]
dt = 1e-3
epsilon = 0.01
endTime = 0.5

import math
import ufl

def projection(Vec):
    dim = Vec.ufl_shape[0]
    return Vec- (1/dim)*ufl.as_vector(dim*[sum(Vec)])

def initial(x):
    phase1 = 0.5+0.5*math.tanh( (2/epsilon)  * min(  math.sqrt(x[0]*x[0]+x[1]*x[1] ) -0.1, x[1]))
    phase2 = 0.5-0.5*math.tanh( (2/epsilon) * x[1])
    phase3 = 1-phase1-phase2
    return [ phase1  , phase2  , phase3  ]

import dune.fem as fem
from dune.grid import Marker, cartesianDomain
import dune.create as create
order = 1
dimDomain = 2     # we are solving this in 2d
dimRange = 3      # we have a system with two unknowns
domain = cartesianDomain([-0.2, -0.2], [0.2, 0.2], [3, 3])
grid   = create.view("adaptive", grid="ALUConform",
                    constructor=domain, dimgrid=dimDomain)
space  = create.space("lagrange", grid, dimRange=dimRange,
                order=order, storage="fem")

from ufl import TestFunction, TrialFunction, Coefficient, Constant, triangle
u = TrialFunction(space)
v = TestFunction(space)
un = Coefficient(space)

initial_gf = create.function("global", grid, "initial", order+1, initial)
solution   = space.interpolate(initial_gf, name="solution")
solution_n = solution.copy()

Sigma1 = Gamma[0]+Gamma[1]-Gamma[2]
Sigma2 = Gamma[0]+Gamma[2]-Gamma[1]
Sigma3 = Gamma[1]+Gamma[2]-Gamma[0]

#implicit discretization of the potential wells
#d1  = Sigma1 * u[0]*(1-u[0]) * (1-2*u[0])
#d2  = Sigma2 * u[1]*(1-u[1]) * (1-2*u[1])
#d3  = Sigma3 * u[2]*(1-u[2]) * (1-2*u[2])

sigmaPlus = ufl.as_vector([max(Sigma1,0), max(Sigma2,0), max(Sigma3,0)])

sigmaMinus = ufl.as_vector([-min(Sigma1,0), -min(Sigma2,0), -min(Sigma3,0)])

def fplus(x):
    return (x-0.5)*(x-0.5)*(x-0.5)*(x-0.5)

def fminus(x):
    return (1/16)*(1-2*(2*x-1)*(2*x-1))

coeff = ufl.Coefficient(External.uflSpace(2, 3))

convex = 0.5*ufl.dot(sigmaPlus,ufl.as_vector([fplus(coeff[0]),fplus(coeff[1]),fplus(coeff[2])])) - 0.5*ufl.inner(sigmaMinus,ufl.as_vector([fminus(coeff[0]),fminus(coeff[1]),fminus(coeff[2])]))

convex = 0.5*(sigmaPlus[0]*fplus(coeff[0]) + sigmaPlus[1]*fplus(coeff[1])+ sigmaPlus[2]*fplus(coeff[2]) - sigmaMinus[0]*fminus(coeff[0]) - sigmaMinus[1]*fminus(coeff[1]) -sigmaMinus[1]*fminus(coeff[1]))

convexDash = ufl.algorithms.apply_derivatives.apply_derivatives(ufl.diff(convex, coeff))

convexDash = ufl.replace(convexDash,{coeff:u})
#now replace coeff with u

concave = 0.5*(sigmaPlus[0]*fminus(coeff[0]) + sigmaPlus[1]*fminus(coeff[1])+ sigmaPlus[2]*fminus(coeff[2]) - sigmaMinus[0]*fplus(coeff[0]) - sigmaMinus[1]*fplus(coeff[1]) -sigmaMinus[1]*fplus(coeff[1]))

concaveDash = ufl.algorithms.apply_derivatives.apply_derivatives(ufl.diff(concave, coeff))

#now replace coeff with un
concaveDash = ufl.replace(concaveDash,{coeff:un})

#doublewell = ufl.as_vector([d1,d2,d3])
doublewell = convexDash + concaveDash

Tau = epsilon * mobility*ufl.as_vector([Sigma1, Sigma2, Sigma3  ])
lhs = ufl.inner(ufl.elem_mult(Tau,u-un),v)* ufl.dx

grad_term1 = ufl.inner( Sigma1*ufl.grad(u[0]), ufl.grad(v[0]))
grad_term2 = ufl.inner( Sigma2*ufl.grad(u[1]), ufl.grad(v[1]))
grad_term3 = ufl.inner( Sigma3*ufl.grad(u[2]), ufl.grad(v[2]))
grad_term = (3./4.)*epsilon*(grad_term1+grad_term2+grad_term3)

rhs = grad_term
rhs = rhs + (1/epsilon)*ufl.inner(projection(doublewell),v)
rhs = -dt*rhs*ufl.dx


model  = create.model("integrands", grid, lhs == rhs, coefficients={un:solution_n} )
solverParameters = {"tolerance": 1e-5,
                    "verbose": True,
                    "linear.tolerance": 1e-8,
                    "linear.preconditioning.method": "ilu",
                    "linear.preconditioning.iterations": 1,
                    "linear.preconditioning.relaxation": 1.2,
                    "linear.verbose": True}
scheme = create.scheme("galerkin", model, space, solver="gmres",
        parameters=solverParameters)


# We set up the adaptive method. We start with a marking strategy based on the value of the gradient of the phase field variable:

# In[ ]:


def mark(element):
    solutionLocal = solution.localFunction(element)
    grad = solutionLocal.jacobian(element.geometry.referenceElement.center)
    if grad.infinity_norm > 1.2:
        return Marker.refine if element.level < maxLevel else Marker.keep
    else:
        return Marker.coarsen


maxLevel = 11
hgrid    = grid.hierarchicalGrid
hgrid.globalRefine(6)
for i in range(0,maxLevel):
    hgrid.mark(mark)
    fem.adapt(solution)
    fem.loadBalance(solution)
    solution.interpolate(initial_gf)
    print(grid.size(0),end=" ")
print()

from dune.fem.plotting import plotComponents as plotComponents
import matplotlib.pyplot as pyplot
from dune.fem.function import levelFunction, partitionFunction

vtk = grid.sequencedVTK("threePhase", pointdata=[solution],
       celldata=[levelFunction(grid), partitionFunction(grid)])

t        = 0.0
endTime = 0.05
while t < endTime:
    print(t,grid.size(0)) # ,end="\r")
    solution_n.assign(solution)
    scheme.solve(target=solution)
    t += dt
    marked = hgrid.mark(mark)
    fem.adapt(solution)
    fem.loadBalance(solution)
    vtk()

