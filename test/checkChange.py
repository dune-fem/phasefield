# file that checks benchmark problems against previously computed solutions to check if there are
# any changes

from dune.fem.function import integrate
import sys
import numpy as np
from phasefield import PhaseModel, PhaseStepper
from dune.fem import parameter
from phasefield.auxfun import ConcaveConvex, InterpolateOne, Obstacle, SimpleInterpolate

sys.path.append("..")

import signal

def handler(signum, frame):
    raise TimeoutError("simulation took to long failed")

def mcf():
    from tutorials.mcf import Mcf
    # edit the endtime
    Mcf.endTime = 0.01
    del Mcf.saveStep

    phaseField = PhaseModel(Mcf, epsilon=0.03, dt=0.001)

    fempyBase = PhaseStepper(phaseField, solver = ("direct"))
    solution = fempyBase.solution

    fempyBase.gridSetup(13, 13)

    while fempyBase.time < Mcf.endTime:
        print(fempyBase.time)
        fempyBase.nextTime()
        fempyBase.adapt()

    return integrate(fempyBase.gridView,fempyBase.solution,4)[0]

def mcfOb():
    from tutorials.mcfObstacle import Mcf
    del Mcf.saveStep
    del Mcf.fileBase
    Mcf.endTime = 2e-3

    maxLevel = 18
    dt = 1e-4
    epsilon = 0.02

    phaseField = PhaseModel(Mcf, constrained=True, epsilon=epsilon, dt=dt)
    fempyBase = PhaseStepper(phaseField)
    fempyBase.gridSetup(13, maxLevel)
    fempyBase.defaultRefine = [1.4, 1.2, 4, maxLevel]

    solution = fempyBase.solution
    while fempyBase.time < Mcf.endTime:
        print(fempyBase.time)
        fempyBase.nextTime()
        fempyBase.adapt()

    return integrate(fempyBase.gridView,fempyBase.solution,4)[0]

def tumour():
    from tutorials.tumour import Tumour
    Tumour.endTime = 0.01
    del Tumour.saveStep

    parameter.append( {"fem.verboserank": -1} )
    solverParameters = {"newton.verbose": True,
                        "newton.linear.verbose": True,
                        "newton.linear.tolerance": 1e-9,
                        "newton.linear.preconditioning.method": "ilu",
                        "newton.linear.preconditioning.iterations": 1,
                        "newton.linear.preconditioning.relaxation": 1.2,
                        "newton.lineSearch": "simple",
                        "newton.tolerance": 1e-6}

    uflModel = PhaseModel(Tumour, epsilon=0.01, dt=0.001, constrained = False)
    uflModel.bulkSmoothdict['Q',0] = lambda phi, phiN: [phiN[0] * phiN[0], phiN[1] * phiN[1]]
    fempyBase = PhaseStepper(uflModel, postBulk = [1],
                             solver="gmres", solverParameters=solverParameters)
    fempyBase.defaultRefine = [1.5, 0.15, 0, 16]
    fempyBase.gridSetup(8,18)
    while fempyBase.time < Tumour.endTime:
        print(fempyBase.time)
        fempyBase.nextTime()
        fempyBase.adapt()

    return integrate(fempyBase.gridView,fempyBase.solution,4)[0]

def crystal():
    solverParameters = {
            "newton.tolerance": 1e-8,
            "newton.linear.tolerance": 1e-10,
            "newton.verbose": True,
            "newton.linear.verbose": True
        }

    from tutorials.crystal import Crystal
    Crystal.endTime = 0.01
    phaseField = PhaseModel(Crystal, epsilon = 0.015, dt = 0.0005, thetaSemi = True)

    from phasefield.auxfun import SmoothStep
    phaseField.interpolate = SmoothStep
    fempyBase = PhaseStepper(phaseField, solverParameters)

    fempyBase.gridSetup(12,12)
    while fempyBase.time < Crystal.endTime:
        print(fempyBase.time)
        fempyBase.nextTime()
        fempyBase.adapt()

    return integrate(fempyBase.gridView,fempyBase.solution,4)[0]

def fracture():
    from tutorials.fracture import Fracture
    epsilon = 0.1
    dt = 1e-4
    maxLevel = 16
    Fracture.endTime = 1e-3
    del Fracture.saveStep
    uflModel = PhaseModel(Fracture, epsilon=epsilon, dt=dt, constrained = True)

    eta = 1e-5

    uflModel.bulkSmoothdict['Q', 0] = lambda phi, phin: [eta + phin[0] * phin[0]]
    uflModel.bulkSmoothdict['Q', 1] = lambda phi, phin: [eta + phin[0] * phin[0]]

    uflModel.interpolate = InterpolateOne

    fempyBase = PhaseStepper(uflModel,
                         preBulk = [0,1],
                         solver="cg",
                         storage="istl")
    solution = fempyBase.solution
    fempyBase.indicator = 1-solution[0]
    ### mynote: perhaps call adapt or call 'adapt' method 'refine'...
    fempyBase.defaultRefine = [3e-1, 1e-1, 8, maxLevel]
    fempyBase.gridSetup(10,maxLevel)

    dx = 0.5
    point = Fracture.tipCenter
    point[0] += dx/2
    arrivalTimes = []

    while fempyBase.time < Fracture.endTime:
        print(fempyBase.time)
        fempyBase.nextTime()
        fempyBase.adapt()

    return integrate(fempyBase.gridView,fempyBase.solution,4)[1]

# if this is true runs and saves the output
# false then checks the result of the sims against that output
try:
    benchMark = bool(int(sys.argv[1]))
except IndexError:
    sys.exit("Enter 1 to compuate and save benchmarks, 0 to check against pre computed one")

try:
    myDict = np.load('benchMark.npy', allow_pickle='TRUE').item()
except FileNotFoundError:
    if benchMark == False:
        sys.exit("can't find old benchmarks")
    print("Creating a new dictionary for saving")
    myDict = {}

simList = [crystal, mcf, mcfOb, tumour, fracture]

#tolarence for comparing solutions
tol = 1e-4

signal.signal(signal.SIGALRM, handler)

for currentSim in simList:
    if benchMark == True:
        print("writing output to files")
        myDict[currentSim.__name__] = currentSim()
    else:
        try:
            # set maximum time for simulation as 5min
            signal.alarm(6000)
            if np.abs(myDict[currentSim.__name__] - currentSim()) > tol:
                sys.exit(currentSim.__name__ + " simulation has failed")
            signal.alarm(0)
        except KeyError as e:
            print(e)
            sys.exit("please benchmark before computations")
        except TimeoutError as e:
            print(e)
            sys.exit(str(currentSim) + " failed took too long")
        print("Simulation succeeded")

if benchMark == True:
    np.save('benchMark.npy', myDict)

print("***All tests ran as expected and exiting***")
