import numpy as np
import matplotlib.pyplot as plt


from ufl import as_vector, conditional, Identity, dot, inner, grad, tr, transpose, sqrt, tan, pi, sin, cos
#**************** Functions for plotting *****************
def c(x):
    y = 1-sin((pi*x/2))
    return  (0.752+2.02*x + 0.37*y*y*y)/cos(pi*x/2)

def startTime(resistance):
    mu = 22000
    lam =  22000

    #mu = 563.2
    #lam = 563.2

    Gc = resistance

    a = 25
    b = 100

    K = lam + (2/3)*mu
    E = (9*K*(K-lam))/(3*K-lam)
    nu = 0.5-E/(6*K)

    sigmacrit = sqrt((Gc*E)/((1-nu*nu)*2*b*tan((pi*a)/(2*b))))/c(a/b)

    tension = 6

    return sigmacrit/tension

colorMatch = {16:'green',17:'red',18:'blue',19:'orange'}

myDict = np.load('fracture.npy', allow_pickle='TRUE').item()
for key, value in myDict.items():
    plt.plot(key[3], value, marker='x', label=key)

# plot the exact solution
y= []
for resValue in np.linspace(0, 10, 500):
    y.append(startTime(resValue))
plt.plot(np.linspace(0, 10, 500), y)

plt.legend()
plt.show()

